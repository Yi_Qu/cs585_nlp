import sys
# from scipy.sparse import csr_matrix
import numpy as np
from Eval import Eval
from math import log, exp
from imdb import IMDBdata


class NaiveBayes:
    def __init__(self, data, ALPHA=1.0):
        self.ALPHA = ALPHA
        self.data = data  # training data
        # TODO: Initalize parameters
        self.vocab_len = data.X.shape[1]
        self.count_positive = []
        self.count_negative = []
        self.num_positive_reviews = 0
        self.num_negative_reviews = 0
        self.total_positive_words = 0
        self.total_negative_words = 0
        self.P_positive = 0
        self.P_negative = 0
        self.deno_pos = 0
        self.deno_neg = 0
        self.prob_labels = []
        self.Train(data.X, data.Y)

    # Train model - X are instances, Y are labels (+1 or -1)
    # X and Y are sparse matrices
    def Train(self, X, Y):
        # TODO: Estimate Naive Bayes model parameters
        positive_indices = np.argwhere(Y == 1.0).flatten()
        self.num_positive_reviews = len(positive_indices)
        negative_indices = np.argwhere(Y == -1.0).flatten()
        self.num_negative_reviews = len(negative_indices)
        # positive_bit = ((Y + 1.0)/2)
        # negative_bit = -1*positive_bit + 1

        # self.count_positive = np.ones([1, X.shape[1]])
        self.count_positive = X.toarray()[positive_indices].sum(axis=0)
        # self.count_positive = bitY.dot(X)
        # self.count_negative = np.ones([1, X.shape[1]])
        self.count_negative = X.toarray()[negative_indices].sum(axis=0)
        # self.count_negative = negative_bit.dot(X)

        self.total_positive_words = np.sum(self.count_positive)
        self.total_negative_words = np.sum(self.count_negative)

        self.deno_pos = self.total_positive_words + 1.0
        self.deno_neg = self.total_negative_words + 1.0

        tot_words = self.total_positive_words + self.total_negative_words
        self.P_positive = self.total_positive_words / tot_words
        self.P_negative = self.total_negative_words / tot_words

        return

    # Predict labels for instances X
    # Return: Sparse matrix Y with predicted labels (+1 or -1)
    def PredictLabel(self, X, probThresh=0.5):
        # TODO: Implement Naive Bayes Classification
        pred_labels = []

        sh = X.shape[0]
        for i in range(sh):
            z = X[i].nonzero()
            pos_score = 0
            neg_score = 0
            for j in range(len(z[0])):
                # Look at each feature
                pos_score += log(
                    (self.count_positive[z[1][j]] + self.ALPHA)/self.deno_pos)
                neg_score += log(
                    (self.count_negative[z[1][j]] + self.ALPHA)/self.deno_neg)
            pos_score += log(self.P_positive)
            neg_score += log(self.P_negative)

            if pos_score > neg_score:       # Predict positive
                pred_labels.append(1.0)
            else:                           # Predict negative
                pred_labels.append(-1.0)

        return pred_labels

    def LogSum(self, logx, logy):
        # TODO: Return log(x+y), avoiding numerical underflow/overflow.
        m = max(logx, logy)
        return m + log(exp(logx - m) + exp(logy - m))

    # Predict the probability of each indexed review in sparse matrix text
    # of being positive
    # Prints results
    def PredictProb(self, test, indexes, probThresh=0.5):
        pred_labels = []
        for i in indexes:
            # TODO: Predict the probability of the i_th review in
            # test being positive review
            # TODO: Use the LogSum function to avoid underflow/overflow
            predicted_label = 0
            log_pos_nume = 0.0
            log_neg_nume = 0.0
            log_pos_deno = 0.0
            log_deno = 0.0
            log_neg_deno = 0.0
            z = test.X[i].nonzero()
            for j in range(len(z[0])):
                row = i
                col = z[1][j]
                P_w = (self.count_positive[col] + self.ALPHA) / self.deno_pos
                N_w = (self.count_negative[col] + self.ALPHA) / self.deno_neg
                P_pos_word_tot = test.X[row, col] * P_w
                P_neg_word_tot = test.X[row, col] * N_w
                log_pos_nume += log(P_pos_word_tot)
                log_neg_nume += log(P_neg_word_tot)
            log_pos_nume += log(self.P_positive)
            log_neg_nume += log(self.P_negative)
            log_pos_deno = log_pos_nume
            log_neg_deno = log_neg_nume
            log_deno = self.LogSum(log_pos_deno, log_neg_deno)

            sum_positive = exp(log_pos_nume - log_deno)
            sum_negative = exp(log_neg_nume - log_deno)
            predicted_prob_positive = sum_positive
            predicted_prob_negative = sum_negative

            if predicted_prob_positive > probThresh:
                predicted_label = 1.0
            else:
                predicted_label = -1.0

            # print test.Y[i], test.X_reviews[i]
            # TODO: Comment the line above, and uncomment the line below
            # print(test.Y[i], predicted_label, predicted_prob_positive,
            #       predicted_prob_negative, test.X_reviews[i])
            if probThresh == 0.5:
                print(test.Y[i], predicted_label,
                      predicted_prob_positive, predicted_prob_negative,
                      test.X_reviews[i][:20], '...')
            pred_labels.append(predicted_label)
        return pred_labels

    # Evaluate performance on test data
    def Eval(self, test):
        Y_pred = self.PredictLabel(test.X)
        ev = Eval(Y_pred, test.Y)
        return ev.Accuracy()

    # Evaluate PredictProb
    def EvalPredictProb(self, test):
        self.prob_labels = self.PredictProb(test, range(test.X.shape[0]), 0.70)
        ev = Eval(self.prob_labels, test.Y)
        return ev.Accuracy()

    # Evaluate precision
    def EvalPrecision(self, test):
        # Y_pred = self.PredictLabel(test.X, probThresh)
        # Y_pred = self.PredictProb(test, range(test.X.shape[0]), 0.85)
        # ev = Eval(Y_pred, test.Y)
        ev = Eval(self.prob_labels, test.Y)
        return ev.Precision()

    # Evaluate recall
    def EvalRecall(self, test):
        # Y_pred = self.PredictLabel(test.X, probThresh)
        # Y_pred = self.PredictProb(test, range(test.X.shape[0]), 0.85)
        # ev = Eval(Y_pred, test.Y)
        ev = Eval(self.prob_labels, test.Y)
        return ev.Recall()

    # Print most frequent words
    def FreqWords(self, smooth=4.2):
        print("Calculating most frequent words")
        tot = np.array(self.count_positive + self.count_negative)
        P = [log((i + smooth) / (j + 1))
             for i, j in zip(self.count_positive, tot)]
        N = [log((i + smooth) / (j + 1))
             for i, j in zip(self.count_negative, tot)]
        diffPN = np.array(P) - N
        diffOccr = np.array(self.count_positive - self.count_negative)
        diffOccr2 = np.array(self.count_negative - self.count_positive)
        diffNP = np.array(N) - P
        weights = diffPN**2 * diffOccr
        weights2 = diffNP**2 * diffOccr2
        freq_words_id = np.argsort(weights)
        freq_neg_id = np.argsort(weights2)
        imp_pos_words = list(map(self.data.vocab.GetWord, freq_neg_id[:20]))
        imp_neg_words = list(map(self.data.vocab.GetWord, freq_words_id[:20]))
        pos_and_weight = [str(i) + ' ' + str(j) for i, j in
                          zip(imp_pos_words, weights[freq_neg_id[:20]])]
        neg_and_weight = [str(i) + ' ' + str(j) for i, j in
                          zip(imp_neg_words, weights2[freq_words_id[:20]])]
        print("Top 20 Positive Words")
        print(pos_and_weight)
        print("Top 20 Negative Words")
        print(neg_and_weight)


if __name__ == "__main__":

    print("Reading Training Data")
    traindata = IMDBdata("%s/train" % sys.argv[1])

    print("Reading Test Data")
    testdata = IMDBdata("%s/test" % sys.argv[1], vocab=traindata.vocab)

    print("Computing Parameters")
    nb = NaiveBayes(traindata, float(sys.argv[2]))
    print("Evaluating")
    print("Test Accuracy: ", nb.Eval(testdata))
    print("Test Probability: ", nb.EvalPredictProb(testdata))
    nb.PredictProb(testdata, range(10))
    print("Precision: ", nb.EvalPrecision(testdata))
    print("Recall: ", nb.EvalRecall(testdata))
    nb.FreqWords()
