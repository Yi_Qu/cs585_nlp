import numpy as np


class Eval:
    def __init__(self, pred, gold):
        self.pred = pred
        self.gold = gold

    def Accuracy(self):
        return np.sum(np.equal(self.pred, self.gold)) / float(len(self.gold))

    def Precision(self):
        tp = np.sum(np.equal((self.pred + self.gold), 2.0))
        fp = np.sum(np.equal((self.pred - self.gold), 2.0))
        return tp / (tp + fp)

    def Recall(self):
        tp = np.sum(np.equal((self.pred + self.gold), 2.0))
        fn = np.sum(np.equal((self.gold - self.pred), 2.0))
        return tp / (tp + fn)
